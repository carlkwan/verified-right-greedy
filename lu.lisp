;; Copyright (C) 2024 Carl Kwan
;;
;; LU decomposition in ACL2
;; Summary:
;;      The main definitions in this file are 
;;      - get-U, which accesses the upper triangular part of a matrix
;;      - get-unit-L, which accesses the lower unit trigular part of a matrix
;;      - lu, which computes the LU factorization of a matrix
;;      - nonsingular-leading-principal-submatrices-p, which recognizes
;;        matrices with nonsingular leading principal submatrices
;;      - ge, Gaussian elimination
;;
;;      The main theorems in this file is
;;      - lu-correctness-induction-step, which states
;;              (equal (m* L U) m)
;;        provided that m is an n x n matrix with n > 1 and the principle
;;        submatrix has an LU factorization
;;      - lu-correctness, which states 
;;              (equal (m* L U) m)
;;        provided that m is an n x n matrix with nonsingular leading principal
;;        submatrices
;; Requires:
(in-package "ACL2")
(include-book "mout")
(include-book "mlayers")
(include-book "std/util/bstar" :dir :system)

(local (set-induction-depth-limit 1))
;; Get upper triangular part of a matrix
;; for taking U in LU
;;  [ * * * * * ]     [ * * * * * ]
;;  [ * * * * * ]     [   * * * * ]
;;  [ * * * * * ] --> [     * * * ]
;;      ...               ...
;;  [ * ... * * ]     [         * ] 
(define get-U ((m matrixp))
 :measure (and (col-count m) (row-count m))
 :returns U
 :verify-guards nil
 (b* (((unless (matrixp m)) (m-empty))
      ((if (m-emptyp m)) m)
      ((if (m-emptyp (row-cdr m))) m)
      ((if (m-emptyp (col-cdr m))) (row-cons (row-car m) (mzero (1- (row-count m)) (col-count m)))))
     (row-cons (row-car m)
               (col-cons (vzero (1- (row-count m)))
                         (get-U (row-cdr (col-cdr m))))))
 ///
 ;; For row*-col-car-get-U
 (local (set-induction-depth-limit 2))
 (more-returns
  ;; "Type" theorems
  (U (implies (matrixp m) (equal (row-count U) (row-count m)))
     :name row-count-get-U)
  (U (implies (matrixp m) (equal (col-count U) (col-count m)))
     :name col-count-get-U)
  (U (matrixp U))

  (U (implies (and (matrixp m) (not (m-emptyp (col-cdr m))))
              (not (m-emptyp (col-cdr U))))
     :name m-emptyp-col-cdr-get-U)

  ;; "Unwrapping" theorems
  (U (implies (matrixp m) (equal (row-car U) (row-car m)))
     :name row-car-get-U)
  (U (equal (m-emptyp U) (or (m-emptyp m) (not (matrixp m))))
     :name m-emptyp-get-U)
  (U (implies (and (matrixp m) (not (m-emptyp m)))
              (equal (col-car U) (cons (car (row-car m))
                                       (vzero (1- (row-count m))))))
     :name col-car-get-U)
  (U (implies (and (matrixp m) (not (m-emptyp m)) (matrixp n))
              (equal (row* (col-car U) n)
                     (sv* (car (row-car m)) (col-car n))))
     :name row*-col-car-get-U)
  (U (implies (and (matrixp m))
              (equal (row-car (col-cdr U))
                     (row-car (col-cdr m))))
     :name row-car-col-cdr-get-U))
 (local (set-induction-depth-limit 1))

 ;; Move "unwrapping" to inside get-U
 (defthm row-cdr-col-cdr-get-U
  (implies (matrixp m)
           (equal (row-cdr (col-cdr (get-U m))) (get-U (row-cdr (col-cdr m))))))
 
 (defthm m-emptyp-row-cdr-col-cdr-get-u-when-square
  (implies (and (matrixp m) 
                (equal (col-count m) (row-count m))
                (or (not (m-emptyp (row-cdr m)))
                    (not (m-emptyp (col-cdr m)))))
           (and (not (m-emptyp (row-cdr (get-U m))))
                (not (m-emptyp (col-cdr (get-u m)))))))

 (verify-guards get-u))

;; Get lower triangular part of a matrix
;; for taking L in LU
;; Replace with 1s on the diagonal 
;;  [ * * * * * ]     [ 1         ]
;;  [ * * * * * ]     [ * 1       ]
;;  [ * * * * * ] --> [ * * 1     ]
;;      ...               ...
;;  [ * ... * * ]     [ * ... * 1 ] 
(define get-unit-L ((m matrixp))
 :measure (and (col-count m) (row-count m))
 :returns L
 :verify-guards nil
 (b* (((unless (matrixp m)) (m-empty))
      ((if (m-emptyp m)) m)
      ((if (m-emptyp (row-cdr m))) (row-cons (cons 1 (vzero (1- (col-count m)))) (m-empty)))
      ((if (m-emptyp (col-cdr m))) (row-cons (cons 1 nil) (row-cdr m))))
     (row-cons (cons 1 (vzero (1- (col-count m))))
               (col-cons (col-car (row-cdr m))
                         (get-unit-L (row-cdr (col-cdr m))))))
 ///
 (more-returns
  ;; "Type" theorems
  (L (implies (matrixp m) (equal (row-count L) (row-count m)))
     :name row-count-get-unit-L)
  (L (implies (matrixp m) (equal (col-count L) (col-count m)))
     :name col-count-get-unit-L)
  (L (matrixp L))

  (L (equal (m-emptyp L) (or (m-emptyp m) (not (matrixp m))))
     :name m-emptyp-L)
  (L (implies (and (matrixp m) (not (m-emptyp (col-cdr m))))
              (not (m-emptyp (col-cdr L))))
     :name m-emptyp-col-cdr-get-unit-L)

  ;; "Unwrapping" theorems
  (L (implies (and (matrixp m))
              (equal (col-car (row-cdr L))
                     (col-car (row-cdr m))))
     :name col-car-row-cdr-get-unit-L)
  (L (implies (and (matrixp m) (not (m-emptyp m))) 
              (equal (row-car L) (cons 1 (vzero (1- (col-count m))))))
     :name row-car-get-unit-L))

 (defthm m-emptyp-row-cdr-get-unit-L
  (implies (and (matrixp m) (not (m-emptyp (row-cdr m))))
           (not (m-emptyp (row-cdr (get-unit-L m))))))

 (verify-guards get-unit-L))

;; Move "unwrapping" to inside get-unit-L
(defthm col-cdr-row-cdr-get-unit-L
 (implies (matrixp m)
          (equal (col-cdr (row-cdr (get-unit-L m))) (get-unit-L (col-cdr (row-cdr m)))))
 :hints (("Goal" :in-theory (enable get-unit-L))))

;; Get lower triangular part of a matrix
;; for taking L in LU
;; Do NOT put 1s on the diagonal
;;  [ * * * * * ]     [ *         ]
;;  [ * * * * * ]     [ * *       ]
;;  [ * * * * * ] --> [ * * *     ]
;;      ...               ...
;;  [ * ... * * ]     [ * ... * * ] 
(define get-L ((A matrixp))
 :measure (and (col-count A) (row-count A))
 :returns L
 :verify-guards nil
 (b* (((unless (matrixp A)) (m-empty))
      ((if (m-emptyp A)) A)
      ((if (m-emptyp (col-cdr A))) A)
      ((if (m-emptyp (row-cdr A))) 
           (col-cons (col-car A) (mzero (row-count A) (1- (col-count A))))))
     (row-cons (cons (car (row-car A)) (vzero (1- (col-count A))))
               (col-cons (col-car (row-cdr A))
                         (get-L (row-cdr (col-cdr A))))))
 ///
 (more-returns
  ;; "Type" theorems
  (L (implies (matrixp A) (equal (row-count L) (row-count A)))
     :name row-count-get-L)
  (L (implies (matrixp A) (equal (col-count L) (col-count A)))
     :name col-count-get-L)
  (L (matrixp L))

  (L (equal (m-emptyp L) (or (m-emptyp A) (not (matrixp A))))
     :name m-emptyp-get-L)
  (L (implies (and (matrixp A) (not (m-emptyp (col-cdr A))))
              (not (m-emptyp (col-cdr L))))
     :name m-emptyp-col-cdr-get-L))

 ;; Necessary for verifying guards
 (defthm get-l-col-cons-def
  (implies (and (matrixp A) (not (m-emptyp (col-cdr A))) (not (m-emptyp (row-cdr A))))
           (equal (get-L A)
                  (col-cons (col-car A)
                            (row-cons (vzero (1- (col-count A)))
                                      (get-L (row-cdr (col-cdr A)))))))
  :hints (("Goal" :use ((:instance row-cons-col-cons 
                                   (m (get-L (row-cdr (col-cdr A)))) 
                                   (l (cons (car (row-car A)) (vzero (1- (col-count A)))))
                                   (k (col-car (row-cdr A))))))))
 (verify-guards get-L)
 
 ;; "Unwrapping" theorems
 (defthm col-car-get-L
  (implies (and (matrixp A))
           (equal (col-car (get-L A)) (col-car A))))

 (defthm row-car-get-L
  (implies (and (matrixp A) (not (m-emptyp A))) 
           (equal (row-car (get-L A)) 
                  (cons (car (row-car A)) (vzero (1- (col-count A)))))))

 (defthm col-car-row-cdr-get-L
  (implies (and (matrixp A) (not (m-emptyp A))) 
           (equal (col-car (row-cdr (get-L A)))
                  (col-car (row-cdr A)))))

;; Move "unwrapping" to inside get-L
(defthm col-cdr-row-cdr-get-L
 (implies (matrixp A)
          (equal (col-cdr (row-cdr (get-L A))) (get-L (row-cdr (col-cdr A))))))

;; Move "unwrapping" to inside get-L
(defthm row-cdr-col-cdr-get-L
 (implies (matrixp A)
          (equal (row-cdr (col-cdr (get-L A))) (get-L (row-cdr (col-cdr A)))))))
;; end define


;; Typical "right-looking" LU factorization algorithm
;;
;; Idea sketch: View A = LU as
;;
;; [ alph | a12 ]   [ 1 | 0 ] [ alph | u ]
;; [ ---------- ] = [ ----- ] [ -------- ]
;; [ a21  | A22 ]   [ l | L ] [  0   | U ]
;;
;; Then we want to set:
;;      a21 := l                = a21 / alph
;;      A22 := LU - out-*(l, u) = A22 - out-*(a21, a12)
(define lu ((A matrixp))
 :measure (and (col-count A) (row-count A))
 :verify-guards nil
 (b* (((unless (matrixp A)) (m-empty))
      ((if (m-emptyp A)) A)
      (alph (car (col-car A)))
      ((if (zerop alph)) 
       (mzero (row-count A) 
              (col-count A)))
      ;; A = [ a1 ] = [   1   ] [ a1 ]
      ;;     [ a2 ]   [ a2/a1 ]
      ;;     [ ...]   [  ...  ]
      ((if (m-emptyp (col-cdr A))) 
       (row-cons (list alph) 
                 (sm* (/ alph) 
                      (row-cdr A))))
      ;; Base case:
      ;; m = [ a1 a2 ... ] = [ 1 ] [ a1 a2 ... ]
      ((if (m-emptyp (row-cdr A))) A)

      (a21 (col-car (row-cdr A)))
      (a12 (row-car (col-cdr A)))
      (A22 (col-cdr (row-cdr A)))
      ;; Want to set:
      ;;  a21 = a21 / alpha 
      ;;  A22 = A22 - out-*(a21, a12)
      (a21 (sv* (/ alph) a21))
      (A22 (m+ A22 (sm* -1 (out-* a21 a12)))))
     (row-cons (row-car A)
               (col-cons a21 (lu A22))))
 ///
 (defthm row-count-lu
  (implies (matrixp m)
           (equal (row-count (lu m)) (row-count m))))
 
 (defthm col-count-lu
  (implies (matrixp m)
           (equal (col-count (lu m)) (col-count m))))
 
 (defthm matrixp-of-lu
           (matrixp (lu m))
  :rule-classes :type-prescription)
 
 (verify-guards lu)

 (defthm m-emptyp-lu
  (equal (m-emptyp (lu m))
         (or (m-emptyp m) (not (matrixp m)))))

 (defthm m-emptyp-row-cdr-/-col-cdr-lu
  (implies (and (matrixp m) 
                (equal (col-count m) (row-count m))
                (or (not (m-emptyp (row-cdr m)))
                    (not (m-emptyp (col-cdr m)))))
           (and (not (m-emptyp (row-cdr (lu m))))
                (not (m-emptyp (col-cdr (lu m)))))))

 (defthmd row-car-of-lu
  (implies (and (not (zerop (car (row-car m)))) (matrixp m))
           (equal (row-car (lu m)) (row-car m))))
 
 (defthm row-car-get-u-of-lu
  (implies (and (not (zerop (car (row-car m)))) (matrixp m))
           (equal (row-car (get-u (lu m)))   
                  (row-car m))))

 (defthm lu-correctness-base-case
  (b* ((alph (car (row-car m)))
       (LU   (lu m))
       (L    (get-unit-L LU))
       (U    (get-u LU)))
      (implies (and (matrixp m) 
                    (not (zerop alph)) 
                    (equal (row-count m) 1))
               (equal (m* L U) m)))
  :hints (("Goal" :in-theory (enable lu)))))
;; end define


;; Formal derivation of 'right-looking' LU algorithm
;; Same idea as before: View A = LU as
;;
;; [ alph | a12 ]   [ 1 | 0 ] [ alph | u ]
;; [ ---------- ] = [ ----- ] [ -------- ]
;; [ a21  | A22 ]   [ l | L ] [  0   | U ]
;;
;; Our assumption was that A22 would be handled by the algorithm recursively:
;;      a21 := l                = a21 / alph
;;      A22 := LU - out-*(l, u) = A22 - out-*(a21, a12)
;;
;; This means that if A22 = LU, after the updates, then algorithm suceeds.
;;
(encapsulate
 ()

 (local (defthm row*-col-car-U-row-cdr-L
   (b* ((alph    (car (col-car m)))
        (L       (get-unit-L (lu m)))
        (U       (get-U (lu m))))
       (implies (and (matrixp m) 
                     (not (zerop alph))
                     (equal (col-count M) (row-count M)))
                (equal (row* (col-car U) (row-cdr L))
                       (col-car (row-cdr M)))))
   :hints (("Goal" :in-theory (e/d (lu) (col-car-get-u))))))

 (local
  (defthm LU-expand-twice-2
   (b* ((alph    (car (col-car m)))
        (L       (get-unit-L (lu m)))
        (U       (get-U (lu m)))
        (a21     (col-car (row-cdr m)))
        (a12     (row-car (col-cdr m)))
        (A22     (col-cdr (row-cdr m)))
        (a21     (sv* (/ alph) a21))
        (A22     (m+ A22 (sm* -1 (out-* a21 a12)))))
       (implies (and (matrixp m) 
                     (not (m-emptyp m))
                     (equal (col-count m) (row-count m))
                     (not (m-emptyp (row-cdr M)))
                     (not (zerop alph)))
                (equal (m* L U)
                       (row-cons (row-car m)
                                 (col-cons (col-car (row-cdr m))
                                           (m+ (out-* a21 a12)
                                               (m* (get-unit-L (lu A22)) (get-U (lu A22)))))))))
   :hints (("Goal" :in-theory (enable lu)))))


 ;; Exposing "recursive-ness" by replacing submatrices with calls to LU algorithm
 ;;
 ;; [ alph | a12 ]   [ 1 | 0 ] [ alph | u ]
 ;; [ ---------- ] = [ ----- ] [ -------- ]
 ;; [ a21  | A22 ]   [ l | L ] [  0   | U ]
 ;;
 ;; 
 ;; get-unit-L ( LU (M) ) * get-U ( LU (M) ) =
 ;;
 ;;  [           m01        ]
 ;;  [ -------------------- ]
 ;;  [  m10 | (l o u) + A22 ]


 (local (defthm silly-type-lemma
  (implies (and (matrixp m) 
                (equal (col-count m) (row-count m)) 
                (not (m-emptyp (row-cdr m)))) 
           (and (not (m-emptyp m))      
                (not (m-emptyp (col-cdr m)))))))

 ;; N + M - N = M
 (local (defthm lemma-2
  (implies (and (matrixp m) (matrixp n) (m+-guard m n))
           (equal (m+ n (m+ m (sm* -1 n))) m))))

 (defthm lu-correctness-induction-step
  (b* ((alph (car (col-car A)))
       (LU   (lu A))
       (L    (get-unit-L LU))
       (U    (get-U LU))
       (a21  (col-car (row-cdr A)))
       (a12  (row-car (col-cdr A)))
       (A22  (col-cdr (row-cdr A)))
       (a21  (sv* (/ alph) a21))
       (A22  (m+ A22 (sm* -1 (out-* a21 a12)))))
      (implies (and (matrixp A) 
                    (not (m-emptyp (row-cdr A)))
                    (equal (col-count A) (row-count A))
                    (not (zerop alph))
                    (equal (m* (get-unit-L (lu A22)) (get-U (lu A22))) A22))
               (equal (m* L U) A)))
  :hints (("Goal" :use ((:instance LU-expand-twice-2)
                        (:instance rewrap-matrix))))))
;; end of encapsulation

;; Nonsingular leading principal submatrices
;; Idea sketch: View A = LU as
;; [ alph | a12 ]   
;; [ ---------- ] 
;; [ a21  | A22 ]   
;;
;; Look at Schur complement:
;;      S = A22 - out-*(a21/alph,a12)
;;
;; and create:
;; [ alph | a12 ]   
;; [ ---------- ] 
;; [      | S   ]   
;; 
;; Assuming alph nonzero, S nonsingular iff A nonsingular so recurse along S
(define nonsingular-principal-leading-submatrices-p ((A matrixp))
 :measure (and (row-count A) (col-count A))
 (b* (((unless (matrixp A)) nil)
      ((if (m-emptyp A)) t)
      (alph (car (col-car A)))
      ((if (zerop alph)) nil)
      ((if (or (m-emptyp (row-cdr A))
               (m-emptyp (col-cdr A))))
       t)
      ;; Compute S = A22 - out-*(a21/alph,a12)
      (a21     (col-car (row-cdr A)))
      (a12     (row-car (col-cdr A)))
      (A22     (col-cdr (row-cdr A)))
      (a21/a   (sv* (/ alph) a21))
      (S       (m+ A22 (sm* -1 (out-* a21/a a12)))))
     (nonsingular-principal-leading-submatrices-p S))
 ///
 (defthm lu-correctness
  (b* ((LU (lu A))
       (L  (get-unit-L LU))
       (U  (get-U LU)))
      (implies (and (equal (col-count A) (row-count A))
                    (nonsingular-principal-leading-submatrices-p A))
               (equal (m* L U) A)))))

;; Gaussian elimination
(defun ge (A) (get-U (lu A)))
